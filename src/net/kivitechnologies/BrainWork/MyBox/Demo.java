package net.kivitechnologies.BrainWork.MyBox;

/**
 * Класс, демонстрирующий коробочки
 * 
 * @author Испольнов Кирилл, 16ит18К 
 */
public class Demo {

    private static final String GREEN = "Зеленый";
    private static final String RED = "Красный";

    public static void main(String[] args) {
        HeavyBox[] boxes = new HeavyBox[10];
        generateBoxes(boxes);
        int indexOfBiggest = findBiggestRedBox(boxes);
        System.out.printf("Найдена самая большая коробка - %s%n", boxes[indexOfBiggest]);
    }

    private static void generateBoxes(Box[] boxes) {
        double width, height, depth, weight;
        String color;
        
        for(int i = 0; i < boxes.length; i++) {
            width = (int)(Math.random() * 7) + 3;
            height = (int)(Math.random() * 7) + 3;
            depth = (int)(Math.random() * 7) + 3;
            
            color = Math.random() > 0.5 ? RED : GREEN;
            weight = Math.random() * 100;
            
            boxes[i] = new HeavyBox(width, height, depth, color, weight);
            System.out.printf("Создана новая коробочка: %s%n", boxes[i]);
        }
    }
    
    private static int findBiggestRedBox(HeavyBox[] boxes) {
        int indexOfBiggest = 0;
        for(int i = 1; i < boxes.length; i++) {
            if(boxes[i].getColor().equals(RED) && boxes[i].getWeight() > boxes[indexOfBiggest].getWeight())
                indexOfBiggest = i;
        }
        
        return indexOfBiggest;
    }
}
