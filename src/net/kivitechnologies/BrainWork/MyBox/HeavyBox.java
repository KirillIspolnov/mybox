package net.kivitechnologies.BrainWork.MyBox;

/**
 * Класс, представляющий коробочку в которую можно складывать вещи
 * Обладает полем weight - вес коробочки и геттером для этого поля
 * 
 * @author Испольнов Кирилл, 16ит18К 
 */
public class HeavyBox extends ColorBox {
    /*
     * Вес коробки 
     */
    private double weight;
    
    /**
     * Конструктор, инициализирующий коробочку заданными размерами, цветом и весом 
     * 
     * @param width ширина
     * @param height высота
     * @param depth глубина
     * @param color цвет
     * @param weight вес
     */
    public HeavyBox(double width, double height, double depth, String color, double weight) {
        super(width, height, depth, color);
        this.weight = weight;
    }
    
    /**
     * Возвразает вес коробки
     * 
     * @return вес коробки
     */
    public double getWeight() {
        return this.weight;
    }
    
    @Override
    public String toString() {
        return String.format("%s имеет вес %.3f", super.toString(), weight);
    }
}
