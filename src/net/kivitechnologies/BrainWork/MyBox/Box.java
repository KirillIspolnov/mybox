package net.kivitechnologies.BrainWork.MyBox;

/**
 * Класс, представляющий коробочку в ее самом простом виде
 * Обладает тремя полями - ширина, высота, глубина, геттерами для этих полей
 * И методом для нахождения объема коробки
 * 
 * @author Испольнов Кирилл, 16ит18К 
 */
public class Box {
    /*
     * Ширина, высота и глубина 
     */
    private double width, height, depth;
    
    /**
     * Конструктор, инициализирующий коробочку заданными размерами
     * 
     * @param width ширина
     * @param height высота
     * @param depth глубина
     */
    public Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    /**
     * Конструктор, инициализирующий кубическую коробочку, т.е. коробку с равными по длине сторонами
     * 
     * @param side длина стороны
     */
    public Box(double side) {
        this(side, side, side);
    }
    
    /**
     * Конструктор по умолчанию
     * Инициализирует кубическую коробочку с длиной стороны, равной единице
     */
    public Box() {
        this(1);
    }
    
    /**
     * Вовращает ширину коробочки
     * 
     * @return ширина коробочки
     */
    public double getWidth() {
        return width;
    }
    
    /**
     * Возвращает высоту коробочки
     * 
     * @return высота коробочки
     */
    public double getHeight() {
        return height;
    }

    /**
     * Возвращает глубину коробочки
     * 
     * @return глубина коробочки
     */
    public double getDepth() {
        return depth;
    }
    
    /**
     * Находит и возвращает объем коробочки
     * 
     * @return объем коробочки
     */
    public double capacity() {
        return width * height * depth;
    }
    
    @Override
    public String toString() {
        return String.format("Коробка с размерами %.3fx%.3fx%.3f имеет объем %.3f", width, height, depth, capacity());
    }
}
