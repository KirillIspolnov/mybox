package net.kivitechnologies.BrainWork.MyBox;

/**
 * Класс, представляющий цветную коробочку 
 * Обладает полем цвет и геттером для этого поля
 * 
 * @author Испольнов Кирилл, 16ит18К 
 */
public class ColorBox extends Box {
    /*
     * Цвет
     */
    private String color;
    
    /**
     * Конструктор, инициализирующий коробочку заданными размерами и цветом
     * 
     * @param width ширина
     * @param height высота
     * @param depth глубина
     * @param color цвет
     */
    public ColorBox(double width, double height, double depth, String color) {
        super(width, height, depth);
        this.color = color;
    }
    
    /**
     * Возвращает цвет коробочки
     * 
     * @return цвет коробочки 
     */
    public String getColor() {
        return color; 
    }
    
    @Override
    public String toString() {
        return super.toString() + ", имеющая цвет \"" + color + "\"";
    }
}
